package com.airfox.androidtest;

import android.app.Activity;
import android.app.IntentService;
import android.content.Intent;
import android.content.Context;
import android.icu.text.LocaleDisplayNames;
import android.os.Bundle;
import android.os.ResultReceiver;
import android.util.Log;

/**
 * An {@link IntentService} subclass for handling asynchronous task requests in
 * a service on a separate handler thread.
 * <p/>
 * helper methods.
 */
public class FibonacciIntentService extends IntentService {
    // IntentService can perform, e.g. ACTION_FETCH_NEW_ITEMS
    private static final String ACTION_FIBONACCI = "fibonacci";
    private static final String EXTRA_NUMBER = "number";
    private static final String EXTRA_RECEIVER = "result_receiver";

    public FibonacciIntentService() {
        super("FibonacciIntentService");
    }

    /**
     * Starts this service to perform action Foo with the given parameters. If
     * the service is already performing a task this action will be queued.
     *
     * @see IntentService
     */
    public static void startActionFibonacci(Context context, int number, FibonacciResultReceiver receiver) {
        Intent intent = new Intent(context, FibonacciIntentService.class);
        intent.setAction(ACTION_FIBONACCI);
        intent.putExtra(EXTRA_NUMBER, number);
        intent.putExtra(EXTRA_RECEIVER, receiver);
        context.startService(intent);
    }


    @Override
    protected void onHandleIntent(Intent intent) {
        if (intent != null) {
            final String action = intent.getAction();
            if (ACTION_FIBONACCI.equals(action)) {
                final int param1 = intent.getIntExtra(EXTRA_NUMBER, 0);
                ResultReceiver fibonacciResultReceiver = intent.getParcelableExtra(EXTRA_RECEIVER);
                Bundle bundle = new Bundle();
                bundle.putLong(MainActivity.RESULT_NUMBER,fibonacci(param1));
                fibonacciResultReceiver.send(Activity.RESULT_OK,bundle);
            }
        }
    }
    /*To improve the execution if fibonacci function we can separate each
    * call of fibonacci() in a separate thread and use join to wait for the previous call
     * or we can use RecursiveAction provided with java 7 */
    private long fibonacci(int n) {
        if (n <= 1) return n;
        else return fibonacci(n - 1) + fibonacci(n - 2);
    }
}

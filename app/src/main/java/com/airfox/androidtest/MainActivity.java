package com.airfox.androidtest;

import android.os.Handler;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

public class MainActivity extends AppCompatActivity {

    public static final String RESULT_NUMBER = "Result";

    private Button mButton;
    private EditText mEditText;
    private TextView mTextView;

    private int value;

    private FibonacciResultReceiver mFibonacciResultReceiver;

    private final View.OnClickListener mSubmitOnlickListener = new View.OnClickListener() {
        @Override
        public void onClick(View view) {
            startService();
        }
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);


        mButton = (Button) findViewById(R.id.button);
        mButton.setOnClickListener(mSubmitOnlickListener);
        mEditText = (EditText) findViewById(R.id.editText);
        mTextView = (TextView) findViewById(R.id.textView);
        setupReceiver();
    }

    private void startService() {
        if (mEditText.getText() != null) {
            value = Integer.parseInt(mEditText.getText().toString());
            FibonacciIntentService.startActionFibonacci(this, value, mFibonacciResultReceiver);
        }
    }

    private void setupReceiver() {
        mFibonacciResultReceiver = new FibonacciResultReceiver(new Handler());

        mFibonacciResultReceiver.setReceiver(new FibonacciResultReceiver.Receiver() {
            @Override
            public void onReceiveResult(int resultCode, Bundle resultData) {
                if (resultCode == RESULT_OK) {
                    long result = resultData.getLong(RESULT_NUMBER);
                    mTextView.setText("Fibonacci of "+value+" : " + result);
                }
            }
        });
    }
}
